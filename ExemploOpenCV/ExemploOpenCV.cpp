#include "stdafx.h"
#include <opencv2/opencv.hpp>
#include <opencv\highgui.h>

using namespace cv;

int main() {

	//Cria a Matrix para armazenar a imagem; 
	Mat image;

	//Inicia a captura;               
	VideoCapture cap;
	cap.open(0);

	//Cria a janela para mostrar;
	namedWindow("window", 1);
	while (1) {
		//Copia a camera para a imagem;
		cap >> image;
		//Mostra a imagem na tela;           
		imshow("window", image);
		//Aguarda 33ms
		waitKey(33);

	}
	return 0;
}